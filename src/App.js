import React from "react";
import moment from "moment";
import TodoHeader from "./components/TodoHeader";
import TodoList from "./components/TodoList";
import Timer from "./components/Timer";
import TodoForm from "./components/TodoForm";
import "./App.css";

class TodoApp extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
        todoItems: [],
        showTimer: false
    };
  }

  addItem = (todoItem) => {
    const { todoItems } = this.state;
    // Make a copy of the state perform mutations on copy state should be immutable
    const newTodoItems = [...todoItems];
    newTodoItems.unshift({
      index: newTodoItems.length + 1,
      value: todoItem,
      date: moment().format("ll"),
      done: false
    });
    this.setState({ todoItems: newTodoItems });
  };

  removeItem = (itemIndex) => {
    const { todoItems } = this.state;
    const newTodoItems = [...todoItems];
    newTodoItems.splice(itemIndex, 1);
    this.setState({ todoItems: newTodoItems });
  };

  markTodoDone = (itemIndex) => {
    const { todoItems } = this.state;
    const newTodoItems = [...todoItems];
    const todo = newTodoItems[itemIndex];
    newTodoItems.splice(itemIndex, 1);
    todo.done = !todo.done;
    todo.done ? newTodoItems.push(todo) : newTodoItems.unshift(todo);
    this.setState({ todoItems: newTodoItems });
  }

  render() {
    const { showTimer, todoItems } = this.state;
    return (
      <div id="main">
        <TodoHeader />
        <button onClick={() => this.setState({ showTimer: !showTimer })}> Toggle Timer </button>

        {showTimer ? <Timer /> : null}

        <TodoList
          items={todoItems}
          removeItem={this.removeItem}
          markTodoDone={this.markTodoDone}
        />
        <TodoForm addItem={this.addItem} />
      </div>
    );
  }
}

export default TodoApp;
