import React from "react";

class Timer extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            count: 0
        }
        this.timer = null;
    }

    componentDidMount = () => {
        this.timer = setInterval(this.updateTimer, 1000);
    }

    componentWillUnmount = () => {
        clearTimeout(this.timer);
    }

    updateTimer = () => {
        const { count } = this.state;
        this.setState({ count: count + 1 });
    }

    render = () => {
        const { count } = this.state;
        return (
            <div>
                <h2>Seconds so Far:</h2>
                <p>{ count }</p>
            </div>
        );
    }
}

export default Timer;
