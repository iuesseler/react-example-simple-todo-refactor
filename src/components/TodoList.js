import React from "react";
import TodoListItem from "./TodoListItem";

const TodoList = ({ items, removeItem, markTodoDone }) => {
    const listItems = items.map((item, index) => {
      return (
        <TodoListItem
          key={index}
          item={item}
          index={index}
          removeItem={removeItem}
          markTodoDone={markTodoDone}
        />
      );
    });
    return <ul className="list-group"> {listItems} </ul>;
}

export default TodoList;
