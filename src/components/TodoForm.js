import React from "react";

class TodoForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
        itemNameValue: ''
    }
  }

  onInputChange = (event) => {
    this.setState({ itemNameValue: event.target.value });
  };

  onSubmit = (event) => {
    const { itemNameValue } = this.state;
    const { addItem } = this.props;
    addItem(itemNameValue);
    this.setState({ itemNameValue: '' });
    event.preventDefault();
  };

  render() {
    const { itemNameValue } = this.state;
    return (
      <form onSubmit={this.onSubmit} className="form-inline">
        <input
          type="text"
          id="itemName"
          className="form-control"
          placeholder="add a new todo..."
          autoFocus
          value={ itemNameValue }
          onChange={ this.onInputChange }
        />
        <button type="submit" value="Submit" className="btn btn-default">
          Add
        </button>
      </form>
    );
  }
}

export default TodoForm;
