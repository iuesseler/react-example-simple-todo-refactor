import React from "react";

class TodoListItem extends React.Component {

  onClickClose = () => {
    const { index, removeItem } = this.props;
    const indexNum = parseInt(index, 10);
    return removeItem(indexNum);
  };

  onClickDone = () => {
    const { index, markTodoDone } = this.props;
    const indexNum = parseInt(index, 10);
    return markTodoDone(indexNum);
  };

  render() {
    const { item } = this.props;
    const todoClass = item.done ? "todoItem done" : "todoItem undone";
    return (
      <li className="list-group-item ">
        <div className={todoClass}>
          <span
            className="glyphicon glyphicon-ok icon"
            aria-hidden="true"
            onClick={this.onClickDone}
          />
          <span>{item.value}</span>
          <span className='date'>{`Added: ${item.date}`}</span>
          <button type="button" className="close" onClick={this.onClickClose}>
            &times;
          </button>
        </div>
      </li>
    );
  }
}

export default TodoListItem;
