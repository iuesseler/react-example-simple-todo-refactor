# Front End Refactoring Challenge

You can do whatever you think is best:
install libraries, add/remove/reorganize files, etc.
Just use your best judgment to help us in making the code better and easier to maintain.

Notes:

- Clone this repo. `npm install`, then `npm start` to get your development environment running.
- Node version 9.9.0.

Changes:

- Broke out applications into components.

- Converted unnecessary class based components to functional components with destructed props as arguments.

- Used object destructing `const { todoItems } = this.state` to make code concise and readable.

- Converted most vars to constants.

- Removed jQuery.

- Input value controlled by TodoForm state (Controlled Component).

- Ref removed, not needed.

- componentWillUnmount life cycle method added on Timer component to clearTimeout

- State immutable, make shallow copy with `[...copy]` spread operator.

- Removed unnecessary this.handleBlank.bind(this) in constructor context is kept by fat arrow functions () =>.
